/*******************************************************************************
* $Id: Object.js 20552 2020-11-28 21:48:55Z sthames $ */
/**
* @ngdoc       object
* @name        Object
* @description Object polyfills.
*******************************************************************************/
(function() { 'use strict';
  
/*******************************************************************************
* Object.combine */
/**
* @ngdoc    method
* @name     combine
* @methodOf polyfill.Object
* @param    {object}    target     Target object.
* @param    {object...} arguments  Source objects.
* @return   {object}               Modified `target` object.
* @description
* Performs the same function as `Object.assign` but copies property definitions
* rather than property values. Copy is non-destructive so, as the properties of
* each source object are copied to `target`, any properties in `target` will not 
* be overwritten.
*
* see: https://webreflection.co.uk/blog/2015/10/06/how-to-copy-objects-in-javascript
*******************************************************************************/
Object.combine = function(target)
  {
  for (var i=1; i < arguments.length; i++)
    Object.getOwnPropertyNames(arguments[i]).forEach(function(key)
      {
      if (!Object.prototype.hasOwnProperty(target, key))
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(this, key));
      }, arguments[i]);
  return target;
  };
})();