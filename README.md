# WebSpinnerAJS

| | |
|-|-|
| **Package Name** | @slcpub/webspinner-ajs |
| **Description**  | Javascript Utility Library for [AngularJS][angularjs]. |
| **Module Name**  | wsajs |

- **Library Files**
  - webspinner-ajs.js
  - webspinner-ajs.min.js
  
This package is maintained for both **`npm`** and **`bower`** using **[`csm-build-tools`]** which combines 
the AngularJS code files into a single Javascript file and all the style files into a single CSS file. 
These files are loaded from the installed package location in **`node_modules`** or **`bower_components`**.

Each subfolder of **`src`** represents an AngularJS module and the enclosed Javascript (.js) file
with the subfolder name creates the module. For example, `src/data/data.js` creates the `wsajs.data` 
module and all Javascript files, CSS files, and HTML template files (*.template.html) become part of 
the module. All other files are ignored.

[angularjs]: https://angularjs.org/
[`csm-build-tools`]: https://github.com/CSM-Consulting/csm-build-tools

## See Also

  - [Building a Package for npm and Bower.](https://gitlab.com/slcon/pub/doc/-/blob/trunk/BuildPackageForNPMandBower.md)

