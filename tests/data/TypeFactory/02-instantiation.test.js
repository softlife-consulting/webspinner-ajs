﻿/*******************************************************************************
* Written by Steve Thames                                              1/22/2021
* ------------------------------------------------------------------------------
* TypeFactory instantiation tests.
*******************************************************************************/
require('./_common.js');

describe('Instantiation',function() {
  var Data;

  beforeEach(() => {
    angular.mock.module('wsajs.data', (TypeFactoryProvider) => {
      
      /*--------------------------------------------------*/
      /* Data.Person - Default constructor with defaults. */
      /*--------------------------------------------------*/
      TypeFactoryProvider.addType({
        name:     'Data.Person',
        $type:    'Person',
        defaults: {
          name: 'Steve',
          age:  58
        },
        prototype: {
          address: null,
          ssn:     null,
          toJSON:  function() { 
            var    rv = this.defaultToJSON();
            delete rv.ssn;
            return rv;
          }
        }
      });

      /*--------------------------------------------------------*/
      /* Data.Child - Inherit defaults, `toJSON` removes `ssn`. */
      /*--------------------------------------------------------*/
      TypeFactoryProvider.addType({
        name:      'Data.Child',
        inherits:  'Data.Person',
        $type:     'Child',
        prototype: {
          address: null,
          ssn:     null
        }
      });

      /*--------------------------------------*/
      /* Data.GrandChild - Inherits `toJSON`. */
      /*--------------------------------------*/
      TypeFactoryProvider.addType({
        name:      'Data.GrandChild',
        inherits:  'Data.Child',
        $type:     'GrandChild'
      });

      /*------------------------------------------------------*/
      /* Data.NoType - No `$type` and `toJSON` removes `ssn`. */
      /*------------------------------------------------------*/
      TypeFactoryProvider.addType({
        name:      'Data.NoType',
        defaults:  {
          name:    'Steve',
          age:     58,
          ssn:     '999-99-9999'
        },
        prototype: {
          toJSON:  function() { 
            var    rv = this.defaultToJSON();
            delete rv.ssn;
            return rv;
          }
        }
      });
    });

    angular.mock.inject((_Data_) => Data = _Data_);
  });

  test('Default constructor with defaults', () => {
    var p = new Data.Person();
    expect(p instanceof Data.Person).toBe(true);
    expect(p.age).toBe(58);
  });

  test('Stringify as property of another object.', () => {
    var o = { who: new Data.Person() };
    expect(JSON.stringify(o)).toBe('{"who":{"name":"Steve","age":58,"$type":"Person"}}');
  });

  test('Inherits `toJSON`.', () => {
    var g = new Data.GrandChild({name:"Ariana", age:2, ssn:"123-45-6789"});
    expect(JSON.stringify(g)).toBe('{"name":"Ariana","age":2,"$type":"GrandChild"}');
  });

  test('Inherits defaults, stringify includes `$type` and non-destructive', () => {
    var c = new Data.Child({address:'1234 Anystreet', ssn:'555-55-5555'});
    expect(c instanceof Data.Person).toBe(true);
    expect(c instanceof Data.Child).toBe(true);
    expect(JSON.stringify(c)).toBe('{"name":"Steve","age":58,"address":"1234 Anystreet","$type":"Child"}');
    expect(c.ssn).toBe('555-55-5555');
  });

  test('No `$type`, stringify non-destructive', () => {
    var notype = new Data.NoType();
    expect(JSON.stringify(notype)).toBe('{"name":"Steve","age":58}');
    expect(notype.ssn).toBe('999-99-9999');
  });
});
