﻿/*******************************************************************************
* Written by Steve Thames                                              1/22/2021
* ------------------------------------------------------------------------------
* TypeFactory configuration tests. configuration happens in the AngularJS config 
* phase, where types are defined, and the run phase, where they are created. 
* TypeFactoryProvider `addType` and `$get` methods are mocked so exceptions can 
* be captured and `angular.mock.inject()` is called to fire the config and run 
* phases. `expect().toHaveThrown()` is added by this module and used to check 
* the results of calls to `addType` and `$get`.
*******************************************************************************/
require('./_common.js');

/*******************************************************************************
* expect().toHaveThrown */
/**
* Checks all the results of calls to a mocked method to verify an exception was 
* thrown. `message` may be empty (any exception), a Regular Expression, or a
* text fragment to search for in the exception text.
*
* @param  {MockFn}        mockFn  - Mock function.
* @param  {string|RegExp} message - Exception message fragment or regexp.
*                                   `null`=any exception.
*******************************************************************************/
expect.extend({ 
  toHaveThrown(mockFn, message) {
    var ex   = mockFn.mock.results.filter((r) => r.type === 'throw').pop();
    var pass = { pass: true,  message: () => `${mockFn.getMockName()} threw "${ex.value}"` };
    var fail = { pass: false, message: () => `expected ${mockFn.getMockName()} to throw ${!!message ? '"'+message+'"' : 'an exception'}.` };
    
    if (ex)
      if (!message)
        return pass;
      else if (message instanceof RegExp && message.test(ex.value))
        return pass;
      else if (ex.value.indexOf(message) >= 0)
        return pass;
    return fail;
  }
});

/*******************************************************************************
* mockProvider */
/**
* Adds a config function to mock TypeFactoryProvider `addType` and `$get` 
* functions, so their exceptions can be captured, and calls `addType(typeSpec)`.
* `angular.mock.inject()` must be called to execute the function. Returns a 
* promise that is resolved when the config function completes.
*
* @param  {object}  typeSpec - Type specification to `TypeFactory`.
* @return {Promise}            Resolved with mocked `TypeFactoryProvider`.
*******************************************************************************/
function mockProvider(typeSpec) {
  return new Promise((resolve) => {
    /*-------------------------------------------------------*/
    /* Grab provider in config phase and mock provider       */
    /* methods. Call `addType` method to trigger exceptions. */
    /*-------------------------------------------------------*/
    angular.mock.module('wsajs.data', function(TypeFactoryProvider) {
      var provider     = TypeFactoryProvider;
      provider.addType = jest.fn().mockName('addType').mockImplementation(provider.addType);
  
      /*------------------------------------------------------------------------------------*/
      /* Service constructor ($get) can't simply be mocked but must be injected using the   */
      /* service injector which is not available until the service is instantiated in the   */
      /* run phase. The old "chicken and egg" problem.                                      */
      /*                                                                                    */
      /* Replace `$get` with a function that will be injected with the service injector     */
      /* ($injector), replace itself with a mock that will the service injector to invoke   */
      /* the actual `$get`, and execute the mock.                                           */
      /*                                                                                    */
      /* There may be a better way to do this but I couldn't think of one. Some help can    */
      /* be found here:                                                                     */
      /* https://medium.com/@a_eife/testing-config-and-run-blocks-in-angularjs-1809bd52977e */
      /*------------------------------------------------------------------------------------*/
      provider.$get = (function($get) {
        return function($injector) {
          provider.$get = jest.fn().mockName('$get').mockImplementation(() => $injector.invoke($get, provider));
          provider.$get();
        };
      })(provider.$get);
  
      /*---------------------------------------------*/
      /* Add the type spec to trigger the exception. */
      /*---------------------------------------------*/
      provider.addType(typeSpec);
  
      resolve(provider);
    });
  });
}

/*******************************************************************************
* Configuration Exception Tests
*
* Each test adds an object Type definition using `TypeFactoryProvider.addType()`
* and verifies the expected exception was thrown.
*******************************************************************************/
describe('Configuration Exceptions', function() {
  /*--------------------------------------------------------------------------------*/
  /* Table passed to `test.each()` containing test function parameters:             */
  /*                                                                                */
  /* 0 - Test Name                                                                  */
  /* 1 - Name of mocked `TypeFactoryProvider` method that will throw the exception. */
  /* 2 - `message` to pass to `expect().toHaveThrown()`.                            */
  /* 3 - Type specification to pass to `TypeFactoryProvider.addType()`.             */
  /*--------------------------------------------------------------------------------*/
  var tests = [
    [ "input 'spec' is not an object.",                      'addType', "input 'spec' must be an object",                       [] ],
    [ "'spec.name' is required.",                            'addType', "'spec.name' is required",                              {} ],
    [ "'spec.name' does not have a service name.",           'addType', "'<serviceName>[.<namespace>].<className>'",            { name: 'MyClass' }],
    [ "'spec.name' has invalid service name.",               '$get',    "is not a valid type service",                          { name: '$location.MyClass' }],
    [ "'spec.inherits' same as class name.",                 'addType', "cannot inherit itself",                                { name: 'Data.MyClass', inherits:    'Data.MyClass' }],
    [ "'spec.inherits' class not found.",                    '$get',    /class '.+?' not found/,                                { name: 'Data.MyClass', inherits:    'Data.MyParent' }],
    [ "'spec.defaults' is not an object or a function.",     'addType', "'defaults' must be an object or injectable function",  { name: 'Data.MyClass', defaults:    'notOorF' }],    
    [ "'spec.defaults' function did not return an object.",  '$get',    "Instance defaults must be an object.",                 { name: 'Data.MyClass', defaults:    () => 'notO' }],
    [ "'spec.defaults' overrides 'spec.compile'.",           '$get',    "'defaults' can't override 'compile' defaults",         { name: 'Data.MyClass', defaults:    angular.noop, compile: () => ({ defaults: {}})}],
    [ "'spec.prototype' is not an object or a function.",    'addType', "'prototype' must be an object or injectable function", { name: 'Data.MyClass', prototype:   'notOorF' }], 
    [ "'spec.prototype' function did not return an object.", '$get',    "Instance prototype must be an object.",                { name: 'Data.MyClass', prototype:   () => 'notO' }],
    [ "'spec.prototype' overrides 'spec.compile'.",          '$get',    "'prototype' can't override 'compile' prototype",       { name: 'Data.MyClass', prototype:   angular.noop, compile: () => ({ prototype: {}})}],
    [ "'spec.constructor' is not a function.",               'addType', "'constructor' must be an injectable function",         { name: 'Data.MyClass', constructor: 'notF' }], 
    [ "'spec.constructor' did not return a function.",       '$get',    "Instance constructor must be a function.",             { name: 'Data.MyClass', constructor: () => 'notF' }],
    [ "'spec.constructor' overrides 'spec.compile'.",        '$get',    "'constructor' can't override 'compile' constructor.",  { name: 'Data.MyClass', constructor: angular.noop, compile: () => ({ constructor: angular.noop })}],
    [ "'spec.compile' is not a function.",                   'addType', "'compile' must be an injectable function",             { name: 'Data.MyClass', compile:     'notF' }], 
    [ "'spec.compile' must return an object.",               '$get',    "'compile' function must return an object.",            { name: 'Data.MyClass', constructor: angular.noop, compile: angular.noop }],
    [ "spec.compile().constructor is not a function.",       '$get',    "Instance constructor must be a function.",             { name: 'Data.MyClass', compile:     () => ({ constructor: 'notF' }) }],
    [ "spec.compile().prototype is not an object.",          '$get',    "Instance prototype must be an object.",                { name: 'Data.MyClass', compile:     () => ({ prototype:   'notO' }) }],
    [ "spec.compile().defaults is not an object.",           '$get',    "Instance defaults must be an object.",                 { name: 'Data.MyClass', compile:     () => ({ defaults:    'notO' }) }],
  ];
  
  /*----------------------------------------------------------------*/
  /* For each test, fire `inject()` to start the config/run phases. */
  /* `inject()` will throw an exception but it's an AngularJS link  */
  /* which is too difficult to parse. Call `.toHaveThrown()` on the */
  /* mocked provider method to get the exception we expect.         */
  /*----------------------------------------------------------------*/
  test.each(tests)('%s', function(name, method, message, typeSpec) {
    mockProvider(typeSpec).then((provider) => expect(provider[method]).toHaveThrown(message));
    expect(() => angular.mock.inject()).toThrow();
  });
});

/*******************************************************************************
* Configuration Exception Tests
*
* Each test adds an object Type definition using `TypeFactoryProvider.addType()`
* and verifies the expected exception was thrown.
*******************************************************************************/
describe('Type Creation', function() {

  test('Injection of "TypeFactory" into "constructor", "defaults", and "prototype" properties.', () => {
    mockProvider({ 
      name:        'Data.Class1', 
      constructor: (TypeFactory, Data) => function(data) { this.defaultConstructor(data); },
      defaults:    (TypeFactory, Data) => TypeFactory.mapObject(Data.Class1)({}),
      prototype:   (TypeFactory, Data) => TypeFactory.mapObject(Data.Class1)({})
    }).then(() => expect(() => angular.mock.inject()).not.toThrow());
  });

  test('Injection of "TypeFactory" into "compiled" property.', () => {
    mockProvider({ 
      name:        'Data.Class1', 
      compiled:    function(TypeFactory, Data) {
        return {
          constructor: function(data) { this.defaultConstructor(data); },
          defaults:    TypeFactory.mapObject(Data.Class1)({}),
          prototype:   TypeFactory.mapObject(Data.Class1)({})
        };
      }
    }).then(() => expect(() => angular.mock.inject()).not.toThrow());
  });
});
